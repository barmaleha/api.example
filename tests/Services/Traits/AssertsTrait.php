<?php

namespace Tests\Services\Traits;

use PHPUnit\Framework\Assert as PHPUnit;

trait AssertsTrait
{
    /**
     * Assert that the array has a given structure.
     *
     * @param  array  $structure
     * @param  array  $responseData
     * @return $this
     */
    public function assertArrayStructure(array $structure, array $responseData)
    {
        foreach ($structure as $key => $value) {
            if (is_array($value) && $key === '*') {
                PHPUnit::assertInternalType('array', $responseData);

                foreach ($responseData as $responseDataItem) {
                    $this->assertArrayStructure($structure['*'], $responseDataItem);
                }
            } elseif (is_array($value)) {
                PHPUnit::assertArrayHasKey($key, $responseData);

                $this->assertArrayStructure($structure[$key], $responseData[$key]);
            } else {
                PHPUnit::assertArrayHasKey($value, $responseData);
            }
        }

        return $this;
    }
}
