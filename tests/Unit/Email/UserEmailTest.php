<?php

namespace Tests\Unit\Email;

use App\Exceptions\User\PasswordNotResetedException;

use App\Models\User;
use App\Services\Email\SendEmailService;
use App\Services\Email\SendUserEmailService;

use DB;
use Illuminate\Support\Str;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class UserEmailTest extends TestCase
{
    private $data = [
        'email' => 'resetpassword@api.loc',
    ];

    public function testHandle_RandomData_UserVerifySent()
    {
        $userMock = Mockery::mock(User::class);
        $userMock->allows('getKey')
            ->andReturn(1);
        $userMock->allows('getEmail')
            ->andReturn($this->data['email']);

        $emailServiceMock = Mockery::mock(SendEmailService::class);
        $emailServiceMock->shouldReceive('send')
            ->andReturn(true)
            ->once();

        $service     = new SendUserEmailService($emailServiceMock);
        $serviceMock = Mockery::mock(UserService::class, $service);

        $serviceMock->sendUserVerify($userMock);
    }

    public function testHandle_RandomData_UserForgotSent()
    {
        // Since we don't know the emailed token from
        // the previous JSON call, we're
        // gonna replace the token with a new one
        $user = User::where('email', $this->data['email'])
            ->first();

        $token = hash_hmac('sha256', Str::random(40), $user);
        DB::table('password_resets')
            ->where(
                'email',
                $this->data['email']
            )
            ->update([
                'token' => password_hash($token, PASSWORD_BCRYPT, ['cost' => '10'])
            ]);

        $userMock = Mockery::mock(User::class);
        $userMock->allows('getEmail')
            ->andReturn($this->data['email']);

        $emailServiceMock = Mockery::mock(SendEmailService::class);
        $emailServiceMock->shouldReceive('send')
            ->andReturn(true)
            ->once();

        $service     = new SendUserEmailService($emailServiceMock);
        $serviceMock = Mockery::mock(UserService::class, $service);

        $serviceMock->sendUserForgotPassword($userMock, $token);
    }

}
