<?php

namespace Tests\Unit\User\Transformer;

use App\Http\Transformers\User\UserTransformer;
use App\Models\User;

use Mockery\MockInterface;
use Tests\Services\Traits\AssertsTrait;
use Tests\TestCase;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class UserTransformerTest extends TestCase
{
    use AssertsTrait;

    public function getUserTransformerInstance(): UserTransformer
    {
        return new UserTransformer();
    }

    public function inputData()
    {
        return [
            [
                [
                    'getKey'           => '6',
                    'getName'          => 'Admin',
                    'getEmail'         => 'admin03@api.loc',
                    'hasVerifiedEmail' => true,
                ],
                '',
                [
                    'data' => [
                        'id',
                        'name',
                        'email',
                        'is_verified',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param array     $user           User info
     * @param string    $includes       Includes for transformer
     * @param array     $structure      Test structure for response
     */
    public function testTransform_Data_ValidStructure(
        array $user,
        string $includes,
        array $structure
    ) {
        $model = \Mockery::mock(User::class)
            ->allows($user)
            ->byDefault();

        $transformer    = $this->getUserTransformerInstance();
        $itemResource   = new \League\Fractal\Resource\Item($model, $transformer);
        $fractalManager = new Manager();

        $fractalManager->parseIncludes($includes);

        $result = $fractalManager->createData($itemResource)
            ->toArray();

        $this->assertArrayStructure(
            $structure,
            $result
        );
    }

}
