<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class DeleteTest extends TestCase
{

    public function inputData()
    {
        return [
            [
                6,
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param int       $id             User id
     */
    public function testHandle_RandomData_DestroyIsCalled(
        int $id
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->shouldReceive('destroy')
            ->andReturn(true)
            ->once();

        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $repositoryMock->delete($id);
    }

}
