<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Illuminate\Database\Eloquent\Collection;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class GetAllTest extends TestCase
{

    public function testHandle_RandomData_AllIsCalled()
    {
        $collectionMock = Mockery::mock(Collection::class);

        $modelMock = Mockery::mock(User::class);
        $modelMock->shouldReceive('all')
            ->andReturn($collectionMock)
            ->once();


        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $repositoryMock->all();
    }

}
