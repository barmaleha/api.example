<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class VerifyTest extends TestCase
{

    public function inputData()
    {
        return [
            [
                6,
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param int       $id             User id
     */
    public function testHandle_RandomData_Verified(
        int $id
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('findOrFail')
            ->andReturn($modelMock);
        $modelMock->allows('hasVerifiedEmail')
            ->andReturn(true);
        $modelMock->allows('markEmailAsVerified')
            ->andReturn($modelMock);
        $modelMock->allows('save')
            ->andReturn(true);

        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $this->assertTrue(true);

        $repositoryMock->verify($id);
    }

    /**
     * @dataProvider inputData
     *
     * @param int       $id             User id
     */
    public function testHandle_RandomData_NotVerified(
        int $id
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('findOrFail')
            ->andReturn($modelMock);
        $modelMock->allows('hasVerifiedEmail')
            ->andReturn(false);
        $modelMock->allows('markEmailAsVerified')
            ->andReturn($modelMock);
        $modelMock->allows('save')
            ->andReturn(true);

        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $this->assertFalse(false);

        $repositoryMock->verify($id);
    }

}
