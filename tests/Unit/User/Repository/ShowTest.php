<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class ShowTest extends TestCase
{

    public function inputData()
    {
        return [
            [
                6,
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param int       $id             User id
     */
    public function testHandle_RandomData_FindOrFailIsCalled(
        int $id
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->shouldReceive('findOrFail')
            ->andReturn($modelMock)
            ->once();

        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $repositoryMock->show($id);
    }

}
