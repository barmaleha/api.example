<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class CreateTest extends TestCase
{

    public function inputData()
    {
        return [
            [
                [
                    User::COLUMN_NAME  => 'Admin',
                    User::COLUMN_EMAIL => 'admin01@api.loc',
                    User::COLUMN_PASS  => '123123',
                ],
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param array     $user           User info
     */
    public function testHandle_RandomData_CreateIsCalled(
        array $user
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->shouldReceive('create')
            ->andReturn($modelMock)
            ->once();


        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $repositoryMock->create($user);
    }

}
