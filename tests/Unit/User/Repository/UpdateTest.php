<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class UpdateTest extends TestCase
{

    public function inputData()
    {
        return [
            [
                6,
                [
                    User::COLUMN_NAME  => 'Admin',
                    User::COLUMN_EMAIL => 'admin03@api.loc',
                    User::COLUMN_PASS  => '123123',
                ],
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param int       $id             User id
     * @param array     $data           User info
     */
    public function testHandle_RandomData_UpdateIsCalled(
        int $id,
        array $data
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('find')
            ->andReturn($modelMock);
        $modelMock->shouldReceive('update')
            ->andReturn($modelMock)
            ->once();

        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $repositoryMock->update($data, $id);
    }

}
