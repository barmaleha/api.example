<?php

namespace Tests\Unit\User\Repository;

use App\Repositories\User\UserRepository;
use App\Models\User;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{

    public function inputData()
    {
        return [
            [
                6,
                '123123',
            ],
        ];
    }

    /**
     * @dataProvider inputData
     *
     * @param int       $id             User id
     * @param string    $password       New password
     */
    public function testHandle_RandomData_PasswordUpdated(
        int $id,
        string $password
    ) {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('findOrFail')
            ->andReturn($modelMock);
        $modelMock->allows('setPass')
            ->andReturn($modelMock);
        $modelMock->allows('setRememberToken')
            ->andReturn($modelMock);
        $modelMock->allows('save')
            ->andReturn(true);

        $repository = new UserRepository($modelMock);
        $repositoryMock = Mockery::mock(UserRepository::class, $repository);

        $this->assertTrue(true);

        $repositoryMock->resetPassword($id, $password);
    }

}
