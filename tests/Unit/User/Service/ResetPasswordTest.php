<?php

namespace Tests\Unit\User;

use App\Exceptions\User\PasswordNotResetedException;

use App\Http\Requests\Api\v1\User\UserResetRequest;
use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Services\User\UserService;

use DB;
use Illuminate\Support\Str;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    private $data = [
        'correct' => [
            UserResetRequest::EMAIL        => 'resetpassword@api.loc',
            UserResetRequest::PASS         => '123123',
            UserResetRequest::PASS_CONFIRM => '123123',
            UserResetRequest::TOKEN        => 'token',
        ],
        'not_correct' => [
            UserResetRequest::EMAIL        => 'test@test.test',
            UserResetRequest::PASS         => '123123',
            UserResetRequest::PASS_CONFIRM => '123123',
            UserResetRequest::TOKEN        => 'token',
        ],
    ];

    public function testHandle_RandomData_Reseted()
    {
        // Since we don't know the emailed token from
        // the previous JSON call, we're
        // gonna replace the token with a new one
        $user = User::where('email', $this->data['correct'][UserResetRequest::EMAIL])
            ->first();

        $token = hash_hmac('sha256', Str::random(40), $user);
        DB::table('password_resets')
            ->where(
                'email',
                $this->data['correct'][UserResetRequest::EMAIL]
            )
            ->update([
                'token' => password_hash($token, PASSWORD_BCRYPT, ['cost' => '10'])
            ]);

        $this->data['correct'][UserResetRequest::TOKEN] = $token;

        $requestMock = Mockery::mock(UserResetRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['correct']);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $repositoryMock->shouldReceive('resetPassword')
            ->andReturn(true)
            ->once();

        $service     = new UserService($repositoryMock);
        $serviceMock = Mockery::mock(UserService::class, $service);

        $serviceMock->reset($requestMock);
    }

    public function testHandle_RandomData_NotReseted()
    {
        $requestMock = Mockery::mock(UserResetRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['not_correct']);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $repositoryMock->allows('resetPassword')
            ->andReturn(true);

        $service     = new UserService($repositoryMock);
        $serviceMock = Mockery::mock(UserService::class, $service);

        $this->expectException(PasswordNotResetedException::class);

        $serviceMock->reset($requestMock);
    }

}
