<?php

namespace Tests\Unit\User;

use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Services\User\UserService;

use Illuminate\Http\Request;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class VerifyUserTest extends TestCase
{

    public function testHandle_RandomData_VerifyIsCalled()
    {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('getKey')
            ->andReturn(1);
        $modelMock->allows('refresh')
            ->andReturn($modelMock);

        $requestMock = Mockery::mock(Request::class);
        $requestMock->allows('input')
            ->andReturn('test');

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $repositoryMock->shouldReceive('verify')
            ->andReturn(true)
            ->once();

        $service = new UserService($repositoryMock);

        $serviceMock = Mockery::mock(UserService::class, $service);

        $serviceMock->verify($requestMock, $modelMock);
    }

}
