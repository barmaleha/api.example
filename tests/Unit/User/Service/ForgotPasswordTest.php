<?php

namespace Tests\Unit\User;

use App\Exceptions\User\ResetEmailNotSentException;

use App\Http\Requests\Api\v1\User\UserForgotRequest;
use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Services\User\UserService;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    private $data = [
        'correct' => [
            UserForgotRequest::EMAIL => 'resetpassword@api.loc',
        ],
        'not_correct' => [
            UserForgotRequest::EMAIL => 'test@test.test',
        ],
    ];

    public function testHandle_RandomData_LinkSent()
    {
        $requestMock = Mockery::mock(UserForgotRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['correct']);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $this->assertTrue(true);

        $serviceMock->forgot($requestMock);
    }

    public function testHandle_RandomData_LinkNotSent()
    {
        $requestMock = Mockery::mock(UserForgotRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['not_correct']);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $this->expectException(ResetEmailNotSentException::class);

        $serviceMock->forgot($requestMock);
    }

}
