<?php

namespace Tests\Unit\User;

use App\Exceptions\User\UserNotAuthorizedException;
use App\Exceptions\User\UserNotVerifiedException;

use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Services\User\UserService;

use Laravel\Passport\Token;
use Illuminate\Http\Request;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;
class LogoutUserTest extends TestCase
{

    public function testHandle_RandomData_LoggedOutApi()
    {
        $tokenMock = Mockery::mock(Token::class);
        $tokenMock->shouldReceive('revoke')
            ->andReturn(true)
            ->once();

        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('getKey')
            ->andReturn(1);
        $modelMock->allows('tokens')
            ->andReturn([$tokenMock]);
        $modelMock->allows('getAttribute')
            ->andReturn([$tokenMock]);

        $requestMock = Mockery::mock(Request::class);
        $requestMock->allows('bearerToken')
            ->andReturn('test');
        $requestMock->allows('user')
            ->andReturn($modelMock);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $serviceMock->logout($requestMock);
    }

    public function testHandle_RandomData_LoggedOutWeb()
    {
        $tokenMock = Mockery::mock(Token::class);
        $tokenMock->shouldReceive('invalidate')
            ->andReturn(true)
            ->once();

        $requestMock = Mockery::mock(Request::class);
        $requestMock->allows('bearerToken')
            ->andReturn(null);
        $requestMock->allows('session')
            ->andReturn($tokenMock);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $serviceMock->logout($requestMock);
    }

}
