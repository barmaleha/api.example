<?php

namespace Tests\Unit\User;

use App\Exceptions\User\UserNotAuthorizedException;
use App\Exceptions\User\UserNotVerifiedException;

use App\Http\Requests\Api\v1\User\UserLoginRequest;
use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Services\User\UserService;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class LoginUserTest extends TestCase
{
    private $data = [
        'correct' => [
            UserLoginRequest::EMAIL => 'admin03@api.loc',
            UserLoginRequest::PASS  => '123123',
        ],
        'not_correct' => [
            UserLoginRequest::EMAIL => 'test@test.test',
            UserLoginRequest::PASS  => 'test',
        ],
        'not_verified' => [
            UserLoginRequest::EMAIL => 'notverified@api.loc',
            UserLoginRequest::PASS  => '123123',
        ],
    ];

    public function testHandle_RandomData_LoggedIn()
    {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('getKey')
            ->andReturn(1);
        $modelMock->allows('hasVerifiedEmail')
            ->andReturn(true);
        $modelMock->allows('refresh')
            ->andReturn($modelMock);

        $requestMock = Mockery::mock(UserLoginRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['correct']);
        $requestMock->allows('filled')
            ->andReturn(false);
        $requestMock->allows('session')
            ->andReturn($requestMock);
        $requestMock->shouldReceive('regenerate')
            ->andReturn(true)
            ->once();

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $serviceMock->login($requestMock);
    }

    public function testHandle_RandomData_NotVerified()
    {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('getKey')
            ->andReturn(1);
        $modelMock->allows('hasVerifiedEmail')
            ->andReturn(false);
        $modelMock->allows('refresh')
            ->andReturn($modelMock);

        $requestMock = Mockery::mock(UserLoginRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['not_verified']);
        $requestMock->allows('filled')
            ->andReturn(false);
        $requestMock->allows('session')
            ->andReturn($requestMock);
        $requestMock->allows('regenerate')
            ->andReturn(true);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $this->expectException(UserNotVerifiedException::class);

        $serviceMock->login($requestMock);
    }

    public function testHandle_RandomData_NotAuthorized()
    {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('getKey')
            ->andReturn(1);
        $modelMock->allows('hasVerifiedEmail')
            ->andReturn(false);
        $modelMock->allows('refresh')
            ->andReturn($modelMock);

        $requestMock = Mockery::mock(UserLoginRequest::class);
        $requestMock->allows('only')
            ->andReturn($this->data['not_correct']);
        $requestMock->allows('filled')
            ->andReturn(false);
        $requestMock->allows('session')
            ->andReturn($requestMock);

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $service        = new UserService($repositoryMock);
        $serviceMock    = Mockery::mock(UserService::class, $service);

        $this->expectException(UserNotAuthorizedException::class);

        $serviceMock->login($requestMock);
    }

}
