<?php

namespace Tests\Unit\User;

use App\Http\Requests\Api\v1\User\UserRequest;
use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Services\User\UserService;

use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class CreateUserTest extends TestCase
{

    public function testHandle_RandomData_CreateIsCalled()
    {
        $modelMock = Mockery::mock(User::class);
        $modelMock->allows('hasVerifiedEmail')
            ->andReturn(true);

        $requestMock = Mockery::mock(UserRequest::class);
        $requestMock->allows('input')
            ->andReturn('test');

        $repositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $repositoryMock->shouldReceive('create')
            ->andReturn($modelMock)
            ->once();

        $service = new UserService($repositoryMock);

        $serviceMock = Mockery::mock(UserService::class, $service);

        $serviceMock->create($requestMock);
    }

}
