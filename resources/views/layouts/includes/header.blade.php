<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar text-light bg-dark">
    <div class="navbar-nav-scroll">
        <ul class="navbar-nav bd-navbar-nav flex-row">
            @include('layouts.includes.menu.top')
        </ul>
    </div>
</header>
