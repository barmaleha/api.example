<ul class="nav section-nav">
    <li class="nav-item toc-entry toc-h2">
        <a class="nav-link" href="#registration">User</a>
        <ul>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#registration">Registration</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#verify">Verify user</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#login">Login user</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#logout">Logout user</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#check_token">Check token</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#refresh_token">Refresh token</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#forgot_password">Forgot password</a>
            </li>
            <li class="nav-item toc-entry toc-h3">
                <a class="nav-link" href="#reset_password">Reset password</a>
            </li>
        </ul>
    </li>
    <li class="nav-item toc-entry toc-h2">
        <a class="nav-link" href="#top">Back to top</a>
    </li>
</ul>
