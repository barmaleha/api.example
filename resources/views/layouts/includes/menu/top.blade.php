<li class="nav-item {{ (\Request::route()->getName() == 'home') ? 'active' : '' }}">
    <a class="nav-link " href="{{ env('APP_URL') }}/">Home</a>
</li>
