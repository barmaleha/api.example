@extends('layouts.full')

@section('page')

    <div class="container">
        <div class="row">

            @include('layouts.includes.error')

            @yield('content')

        </div>
    </div>

@stop
