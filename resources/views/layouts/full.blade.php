<!doctype html>
<html lang="en">
    <head>
        @include('layouts.includes.head')
    </head>
    <body data-spy="scroll" data-target="#navbar_sidebar" data-offset="70">
        @include('layouts.includes.header')

        <div class="container">

            <div id="main" class="row">
                @yield('page')
            </div>

            <footer class="row navbar-fixed-bottom">
                @include('layouts.includes.footer')
            </footer>

        </div>

        @include('layouts.includes.scripts')
    </body>
</html>
