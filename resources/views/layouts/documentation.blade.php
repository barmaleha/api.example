@extends('layouts.full')

@section('page')

    <div class="container bs-docs-container">
        <div class="row flex-xl-nowrap">

            @include('layouts.includes.error')

            <div class="d-none d-md-block col-md-3 bd-toc">
                @include('layouts.includes.sidebar')
            </div>

            <div class="col-12 col-md-9 bd-content" role="main">
                @yield('content')
            </div>

        </div>
    </div>

@stop
