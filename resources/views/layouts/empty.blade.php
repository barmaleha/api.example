<!doctype html>
<html lang="en">
    <head>
        @include('layouts.includes.head')
    </head>
    <body>
        <div class="container">

            <div id="main" class="row">
                @yield('content')
            </div>

        </div>

        @include('layouts.includes.scripts')
    </body>
</html>
