@php
    $intro = [
        'anchor'      => 'login',
        'title'       => 'Login',
        'description' => 'Get oauth token.<br /> Use <code class="highlighter-rouge">access_token</code> from response for <code class="highlighter-rouge">Authorization</code> header value.<br /> Use <code class="highlighter-rouge">refresh_token</code> from response to refresh current <code class="highlighter-rouge">access_token</code>.',
        'method'      => 'POST',
        'uri'         => '/oauth/token',
    ];

    $headers = [
        'ignoreauth' => true,
        'headers'    => [],
    ];

    $body = [
        'params' => [
            [
                'key'         => 'username',
                'value'       => 'admin03@api.loc',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s email address',
            ],
            [
                'key'         => 'password',
                'value'       => '123123',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s password',
            ],
            [
                'key'         => 'grant_type',
                'value'       => 'password',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Type of authorization',
            ],
            [
                'key'         => 'client_id',
                'value'       => '1',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Client ID',
            ],
            [
                'key'         => 'client_secret',
                'value'       => 'Pd1mo9lMhW...',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Client Secret key',
            ],
        ],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"error":"invalid_credentials","message":"The user credentials were incorrect."}',
            'success' => '{"token_type":"Bearer","expires_in":3600,"access_token":"eyJ0eXAiOi...","refresh_token":"def502007..."}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])

