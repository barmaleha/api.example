@php
    $intro = [
        'anchor'      => 'verify',
        'title'       => 'Verify user',
        'description' => 'Verify user by link from email.',
        'method'      => 'GET',
        'uri'         => '/v1/verify/{userID}',
    ];

    $headers = [
        'ignoreauth' => true,
        'headers'    => [],
    ];

    $body = [
        'params' => [],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"status_code":404,"message":"The resource you are looking for could not be found.","info":"ModelNotFoundException"}',
            'success' => '{"id":6,"name":"Admin","email":"admin03@api.loc","is_verified":true}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])
