@php
    $intro = [
        'anchor'      => 'registration',
        'title'       => 'Registration',
        'description' => 'Create user by email and password.',
        'method'      => 'POST',
        'uri'         => '/v1/signup',
    ];

    $headers = [
        'ignoreauth' => true,
        'headers'    => [],
    ];

    $body = [
        'params' => [
            [
                'key'         => 'email',
                'value'       => 'admin03@api.loc',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s email address',
            ],
            [
                'key'         => 'password',
                'value'       => '123123',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s password',
            ],
            [
                'key'         => 'password_confirmation',
                'value'       => '123123',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s password',
            ],
            [
                'key'         => 'name',
                'value'       => 'Admin',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s name',
            ],
        ],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"status_code":422,"message":"The given data was invalid.","errors":{"email":["The email has already been taken."]}}',
            'success' => '{"id":6,"name":"Admin","email":"admin03@api.loc","is_verified":false}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])
