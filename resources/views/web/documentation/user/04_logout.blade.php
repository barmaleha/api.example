@php
    $intro = [
        'anchor'      => 'logout',
        'title'       => 'Logout user',
        'description' => 'Logouts user from system.',
        'method'      => 'GET',
        'uri'         => '/v1/logout',
    ];

    $headers = [
        'headers' => [],
    ];

    $body = [
        'params' => [],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"status_code":404,"message":"The resource you are looking for could not be found.","info":"ModelNotFoundException"}',
            'success' => '{"status":"success","message":"You has been logged out."}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])


