@php
    $intro = [
        'anchor'      => 'forgot_password',
        'title'       => 'Forgot password',
        'description' => 'Send link to email to reset password.',
        'method'      => 'GET',
        'uri'         => '/v1/forgot',
    ];

    $headers = [
        'ignoreauth' => true,
        'headers'    => [],
    ];

    $body = [
        'params' => [
             [
                'key'         => 'email',
                'value'       => 'admin03@api.loc',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s email address',
            ],
        ],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"error_code":306,"status_code":422,"message":"Reset link have not been sent.","errors":[]}',
            'success' => '{"status":"success","message":"Reset email has been sent."}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])


