@php
    $intro = [
        'anchor'      => 'reset_password',
        'title'       => 'Reset password',
        'description' => 'Save new password.',
        'method'      => 'GET',
        'uri'         => '/v1/reset',
    ];

    $headers = [
        'ignoreauth' => true,
        'headers'    => [],
    ];

    $body = [
        'params' => [
             [
                'key'         => 'email',
                'value'       => 'admin03@api.loc',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s email address',
            ],
             [
                'key'         => 'token',
                'value'       => '45f8abff1...',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Token from email link',
            ],
            [
                'key'         => 'password',
                'value'       => '123123',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s password',
            ],
            [
                'key'         => 'password_confirmation',
                'value'       => '123123',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'User\'s password',
            ],
        ],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"error_code":307,"status_code":422,"message":"Password have not been updated.","errors":[]}',
            'success' => '{"status":"success","message":"Password has been reseted."}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])


