@php
    $intro = [
        'anchor'      => 'refresh_token',
        'title'       => 'Refresh token',
        'description' => 'Refresh oauth token.',
        'method'      => 'POST',
        'uri'         => '/oauth/token',
    ];

    $headers = [
        'ignoreauth' => true,
        'headers'    => [],
    ];

    $body = [
        'params' => [
            [
                'key'         => 'refresh_token',
                'value'       => 'def5020082...',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Refresh token from GET TOKEN request',
            ],
            [
                'key'         => 'grant_type',
                'value'       => 'refresh_token',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Type of authorization',
            ],
            [
                'key'         => 'client_id',
                'value'       => '1',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Client ID',
            ],
            [
                'key'         => 'client_secret',
                'value'       => 'Pd1mo9lMhW...',
                'type'        => 'string',
                'is_required' => 'Yes',
                'description' => 'Client Secret key',
            ],
        ],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"error":"invalid_request","message":"The refresh token is invalid.","hint":"Token has been revoked"}',
            'success' => '{"token_type":"Bearer","expires_in":3600,"access_token":"eyJ0eXAiOi...","refresh_token":"def502002..."}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])


