@php
    $intro = [
        'anchor'      => 'check_token',
        'title'       => 'Check token',
        'description' => 'Endpoint to check token. Returns "ok" if token is not expired.',
        'method'      => 'GET',
        'uri'         => '/v1/token/check',
    ];

    $headers = [
        'headers' => [],
    ];

    $body = [
        'params' => [],
    ];

    $response = [
        'responses' => [
            'fail'    => '{"status_code":422,"message":"Route [login] not defined."}',
            'success' => '{"status":"ok"}',
        ],
    ];
@endphp

@include('web.documentation.includes.section', [
        'info'     => $intro,
        'headers'  => $headers,
        'body'     => $body,
        'response' => $response,
    ])


