<div class="bd-callout bd-callout-info">
    <div class="card">
        <div class="card-header">
            Body
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Key</th>
                        <th scope="col">Value</th>
                        <th scope="col">Type</th>
                        <th scope="col">Required</th>
                        <th scope="col">Description</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $json = [];
                    @endphp
                    @forelse ($params as $param)
                        @php
                            $json[$param['key']] = $param['value'];
                        @endphp
                        <tr class="{{ (strtoupper($param['is_required']) == 'YES') ? 'text-primary' : 'text-secondary' }}">
                            <th scope="row">{{ $param['key'] }}</th>
                            <td>{{ $param['value'] }}</td>
                            <td>{{ $param['type'] }}</td>
                            <td>{{ $param['is_required'] }}</td>
                            <td>{{ $param['description'] }}</td>
                        </tr>
                    @empty
                        @include('layouts.includes.norecords')
                    @endforelse
                </tbody>
            </table>

            <figure class="highlight border border-dark">
                <pre><code class="language-js" data-lang="js">{{ json_encode($json, JSON_PRETTY_PRINT) }}</code></pre>
            </figure>
        </div>
    </div>
</div>
