<hr id="{{ $anchor }}" class="mt-5 mb-5 pt-5" />
<h2 class="title mt-5 mb-1">{{ $title }}</h2>
<div class="bd-callout bd-callout-primary">
    <p>{!! $description !!}</p>
    @include('web.documentation.includes.uri', [
            'uri'    => $uri,
            'method' => $method,
        ])
</div>
