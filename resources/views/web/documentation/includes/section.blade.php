@include('web.documentation.includes.intro', $info)
@include('web.documentation.includes.headers', $headers)
@include('web.documentation.includes.body', $body)
@include('web.documentation.includes.response', $response)
