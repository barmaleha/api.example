<div class="bd-callout bd-callout-danger">
    <div class="card">
        <div class="card-header">
            Response
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Type</th>
                        <th scope="col">Response body</th>
                    </tr>
                </thead>
                <tbody>
                     @forelse ($responses as $type => $response)
                        <tr class="{{ (strtoupper($type) == 'SUCCESS') ? 'text-success' : 'text-danger' }}">
                            <th scope="row">{{ ucfirst($type) }}</th>
                            <td>
                                <figure class="highlight border border-dark">
                                    <pre><code class="language-js" data-lang="js">{{ json_encode(json_decode($response), JSON_PRETTY_PRINT) }}</code></pre>
                                </figure>
                            </td>
                        </tr>
                    @empty
                        @include('layouts.includes.norecords')
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
