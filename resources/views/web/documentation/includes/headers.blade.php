@php
    $list = [
        'Content-Type' => 'application/json',
        'Accept'       => 'application/json',
    ];

    if (!isset($ignoreauth)) {
        $list['Authorization'] = 'Bearer eyJ0eXAiOi...';
    }

    $headers = array_merge($list, $headers);
@endphp

<div class="bd-callout bd-callout-warning">
    <div class="card">
        <div class="card-header">
            Headers
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Key</th>
                        <th scope="col">Value</th>
                    </tr>
                </thead>
                    @if (isset($headers))
                        @foreach ($headers as $key => $value)
                            <tr class="{{ (strtoupper($key) == 'AUTHORIZATION') ? 'text-primary' : '' }}">
                                <th scope="row">{{ ucfirst($key) }}</th>
                                <td>{{ $value }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
