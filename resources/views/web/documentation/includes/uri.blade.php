<figure class="highlight border border-dark">
    <pre><code class="language-js" data-lang="js"><strong>{{ $method }}</strong> {{ URL::to('/') . $uri }}</code></pre>
</figure>
