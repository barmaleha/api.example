<h2 class="title">Login [Deprecated]</h2>

<div class="bs-docs-section">
    <pre><strong>POST</strong> /v1/signin</pre>

    <p>
        Login user by credentials
    </p>

    @include('web.documentation.includes.headers')
    @include('web.documentation.includes.body', [
            'params' => [
                [
                    'key'         => 'email',
                    'value'       => 'admin03@api.loc',
                    'type'        => 'string',
                    'is_required' => 'Yes',
                    'description' => 'User\'s email address',
                ],
                [
                    'key'         => 'password',
                    'value'       => '123123',
                    'type'        => 'string',
                    'is_required' => 'Yes',
                    'description' => 'User\'s password',
                ],
                [
                    'key'         => 'remember',
                    'value'       => 'false',
                    'type'        => 'boolean',
                    'is_required' => 'No',
                    'description' => 'Should system remember user',
                ],
            ],
        ])

    @include('web.documentation.includes.response', [
            'responses' => [
                'fail'    => '{"error_code":304,"status_code":422,"message":"Not verified yet.","errors":[]}',
                'success' => '{"id":6,"name":"Admin","email":"admin03@api.loc","is_verified":true}',
            ]
        ])

</div>
