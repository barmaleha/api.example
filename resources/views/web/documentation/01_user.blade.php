@foreach (glob(base_path() . '/resources/views/web/documentation/user/*.blade.php') as $file)
    @include('web.documentation.user.' . basename(str_replace('.blade.php', '', $file)))
@endforeach
