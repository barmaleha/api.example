@extends('layouts.documentation')

@section('content')
    <h1 class="title">Documentation</h1>

    @foreach (glob(base_path() . '/resources/views/web/documentation/*.blade.php') as $file)
        @include('web.documentation.' . basename(str_replace('.blade.php', '', $file)))
    @endforeach
@stop
