<?php

return [

    // Verify email text

    'verify.email.subject' => 'Verify Email Address',
    'verify.email.link.title' => 'Verify Email Address',
    'verify.email.intro' => "Please click the button below to verify your email address.\n If you did not create an account, no further action is required.",
    'verify.email.outro' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n into your web browser: ",

    // Forgot password email text

    'forgot.email.subject' => 'Reset Password Notification',
    'forgot.email.link.title' => 'Reset Password',
    'forgot.email.intro' => "You are receiving this email because we received a password reset request for your account.\n If you did not request a password reset, no further action is required.",
    'forgot.email.outro' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n into your web browser: ",


];
