<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Errors message & code
    |--------------------------------------------------------------------------
    */

    // Service
    101 => 'Access denied',
    102 => 'Bad request',
    103 => 'Internal Server Error',
    104 => 'Not found',
    105 => 'Config not found',
    106 => 'Route not found',
    107 => 'Middleware parameters missing',
    108 => 'An error occurred!',
    109 => 'An error has occurred.',

    // Errors
    201 => 'Whoops, looks like something went wrong',
    202 => 'The resource you are looking for could not be found.',
    203 => 'Unauthenticated',
    204 => 'Method not allowed',
    205 => 'Whoops, looks like something went wrong with routes',
    206 => 'Access denied',

    // User
    301 => 'You has been logged out.',
    302 => 'Reset email has been sent.',
    303 => 'Password has been reseted.',
    304 => 'Not verified yet.',
    305 => 'Authorization failed.',
    306 => 'Reset link have not been sent.',
    307 => 'Password have not been updated.',

];
