<?php

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api\v1\User', ], function () {
    Route::post('signup', 'UserController@create');
    Route::get('verify/{user}', 'UserController@verify');
    Route::post('signin', 'UserController@login');
    Route::post('forgot', 'UserController@forgot');
    Route::post('reset', 'UserController@reset');

    Route::group(['middleware' => 'auth:api', ], function () {
        Route::get('logout', 'UserController@logout');
        Route::get('token/check', 'OauthController@checkToken');
    });
});
