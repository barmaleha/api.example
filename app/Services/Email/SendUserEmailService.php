<?php

namespace App\Services\Email;

use App\Services\Email\SendEmailService;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;
use Mail;

class SendUserEmailService
{

    /** @var SendEmailService Email service */
    private $emailService;

    /**
     * Create a new controller instance.
     *
     * @param SendEmailService      $emailService   User service
     */
    public function __construct(SendEmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    /**
     * Sends verification email to the registered user email
     *
     * @param  MustVerifyEmail      $user           User instance
     */
    public function sendUserVerify(MustVerifyEmail $user)
    {
        $prefix = (Route::current() && Route::current()->getPrefix()) ?? '';

        $this->emailService
            ->send(
                $user->getEmail(),
                Lang::getFromJson('Verify Email Address'),
                'emails.user.verify',
                [
                    'link' => url($prefix . '/verify/' . $user->getKey()),
                ]
            );
    }

    /**
     * Sends forgot password email to the registered user email
     *
     * @param  CanResetPassword     $user           User instance
     * @param  string               $token          Token string
     */
    public function sendUserForgotPassword(CanResetPassword $user, string $token)
    {
        $prefix = (Route::current() && Route::current()->getPrefix()) ?? '';

        $this->emailService
            ->send(
                $user->getEmail(),
                Lang::getFromJson('Reset Password Notification'),
                'emails.user.forgot',
                [
                    'link' => url($prefix . '/reset/' . $token . '/' . $user->getEmail()),
                ]
            );
    }

}
