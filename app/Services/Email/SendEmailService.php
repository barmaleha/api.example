<?php

namespace App\Services\Email;

use Mail;

class SendEmailService
{

    /**
     * Sends email
     *
     * @param  string   $toEmail        Email address
     * @param  string   $subject        Email subject
     * @param  string   $template       Email template file
     * @param  array    $data           Variables for email template
     */
    public function send(
        string $toEmail,
        string $subject,
        string $template,
        array $data
    ) {
        Mail::send($template, $data, function ($message) use ($subject, $toEmail) {
            $message->subject($subject);
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->to($toEmail);
        });
    }

}
