<?php

namespace App\Services\User;

use App\DTO\User\UserDTO;

use App\Exceptions\User\PasswordNotResetedException;
use App\Exceptions\User\ResetEmailNotSentException;
use App\Exceptions\User\UserNotAuthorizedException;
use App\Exceptions\User\UserNotVerifiedException;

use App\Interfaces\Repositories\User\UserRepositoryInterface;

use App\Http\Requests\Api\v1\User\UserForgotRequest;
use App\Http\Requests\Api\v1\User\UserLoginRequest;
use App\Http\Requests\Api\v1\User\UserRequest;
use App\Http\Requests\Api\v1\User\UserResetRequest;

use App\Events\User\UserRegisterEvent;

use App\Models\User;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Verified;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

use Illuminate\Validation\ValidationException;

class UserService
{

    /** @var UserRepositoryInterface User repository */
    private $userRepository;

    /**
     * Create a new UserService instance
     *
     * @param UserRepositoryInterface       $userRepository     User repository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Create record of the user.
     *
     * @param UserRequest           $request
     *
     * @return User
     */
    public function create(UserRequest $request): User
    {
        $dto  = (new UserDTO())->createByRequest($request);
        $user = $this->userRepository
            ->create($dto->toArray());

        event(new UserRegisterEvent($user));

        return $user;
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  Request              $request
     * @param  User                 $user
     *
     * @return User
     */
    public function verify(Request $request, User $user): User
    {
        if ($this->userRepository->verify($user->getKey())) {
            event(new Verified($user));
        }

        $user->refresh();

        return $user;
    }

    /**
     * Handle a login request to the application.
     *
     * @param  UserLoginRequest     $request
     * @return User
     * @throws ValidationException | UserNotVerifiedException | UserNotAuthorizedException
     */
    public function login(UserLoginRequest $request): User
    {
        if (Auth::guard()
            ->attempt(
                $request->only(
                    $request::EMAIL,
                    $request::PASS
                ),
                $request->filled($request::REMEMBER)
            )
        ) {
            $user = Auth::user();
            if (!$user->hasVerifiedEmail()) {
                Auth::guard()
                    ->logout();

                throw new UserNotVerifiedException(trans('api.304'));
            }

            $request->session()
                ->regenerate();

            return $user;
        }

        throw new UserNotAuthorizedException(trans('api.305'));
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request              $request
     * @return bool
     */
    public function logout(Request $request): bool
    {
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userTokens = $request->user()
                ->tokens;

            foreach($userTokens as $token) {
                $token->revoke();
            }
        } else {
            Auth::guard()
                ->logout();

            $request->session()
                ->invalidate();
        }

        return true;
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  UserForgotRequest    $request
     * @return bool
     * @throws ResetEmailNotSentException
     */
    public function forgot(UserForgotRequest $request): bool
    {
        $response = Password::broker()
            ->sendResetLink(
                $request->only($request::EMAIL)
            );

        if ($response != Password::RESET_LINK_SENT) {
            throw new ResetEmailNotSentException(trans('api.306'));
        }

        return true;
    }

    /**
     * Reset password for the given user.
     *
     * @param  UserResetRequest     $request
     * @return bool
     * @throws PasswordNotResetedException
     */
    public function reset(UserResetRequest $request): bool
    {
        $response = Password::broker()
            ->reset(
                $request->only(
                    $request::EMAIL,
                    $request::PASS,
                    $request::PASS_CONFIRM,
                    $request::TOKEN
                ),
                function ($user, $password) {
                    $this->userRepository
                        ->resetPassword(
                            $user->getKey(),
                            $password
                        );

                    event(new PasswordReset($user));

                    Auth::guard()
                        ->login($user);
                }
            );

        if ($response != Password::PASSWORD_RESET) {
            throw new PasswordNotResetedException(trans('api.307'));
        }

        return true;
    }

}
