<?php

namespace App\DTO\User;

use App\Http\Requests\Api\v1\User\UserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserDTO
{
    /** @var null|string User name */
    protected $name;

    /** @var null|string User email */
    protected $email;

    /** @var null|string User pass */
    protected $pass;

    /**
     * Gets user name value
     *
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *  Sets user name value
     *
     * @param null|string $name
     *
     * @return $this
     */
    public function setName($name): UserDTO
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets user email value
     *
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     *  Sets user email value
     *
     * @param null|string $email
     *
     * @return $this
     */
    public function setEmail($email): UserDTO
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Gets user pass value
     *
     * @return null|string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     *  Sets user pass value
     *
     * @param null|string $pass
     *
     * @return $this
     */
    public function setPass($pass): UserDTO
    {
        $this->pass = Hash::make($pass);

        return $this;
    }


    /**
     * Creates DTO by given request.
     *
     * @param UserRequest $request
     *
     * @return $this
     */
    static public function createByRequest(UserRequest $request): UserDTO
    {
        $self = new self();
        $self->setName($request->input($request::NAME))
            ->setEmail($request->input($request::EMAIL))
            ->setPass($request->input($request::PASS));

        return $self;
    }

    /**
     * Return array by this DTO
     *
     * @return array
     */
    public function toArray()
    {
        return [
            User::COLUMN_NAME  => $this->getName(),
            User::COLUMN_EMAIL => $this->getEmail(),
            User::COLUMN_PASS  => $this->getPass(),
        ];
    }
}
