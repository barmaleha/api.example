<?php

namespace App\Interfaces\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Gets all records
     *
     * @return Collection[Model]|null
     */
    public function all(): ?Collection;

    /**
     * Creates new record by given data
     *
     * @param  array    $data       Data of new record
     *
     * @return Model|null
     */
    public function create(array $data);

    /**
     * Updates record by given ID nad provided data
     *
     * @param  array    $data       Data for update record
     * @param  int      $id         Given ID of record
     *
     * @return Model|null
     */
    public function update(array $data, int $id);

    /**
     * Deletes record by given ID
     *
     * @param  int      $id         Given ID of record
     *
     * @return bool|null
     */
    public function delete(int $id): ?bool;

    /**
     * Gets record by given ID
     *
     * @param  int      $id         Given ID of record
     *
     * @return Model|null
     */
    public function show($id);
}
