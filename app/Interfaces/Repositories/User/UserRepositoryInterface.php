<?php

namespace App\Interfaces\Repositories\User;

use App\Interfaces\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface extends RepositoryInterface
{

}
