<?php

namespace App\Listeners\User;

use App\Events\User\UserForgotPasswordEvent;
use App\Services\Email\SendUserEmailService;

class SendEmailPasswordForgot
{

    /** @var SendUserEmailService $sendEmailService */
    private $sendEmailService;

    /**
     * Create a new controller instance.
     *
     * @param SendUserEmailService $sendEmailService
     */
    public function __construct(SendUserEmailService $sendEmailService)
    {
        $this->sendEmailService = $sendEmailService;
    }

    /**
     * Handle the event.
     *
     * @param  UserForgotPasswordEvent  $event
     * @return void
     */
    public function handle(UserForgotPasswordEvent $event)
    {
        $this->sendEmailService
            ->sendUserForgotPassword($event->user, $event->token);
    }
}
