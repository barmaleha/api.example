<?php

namespace App\Listeners\User;

use App\Events\User\UserRegisterEvent;
use App\Services\Email\SendUserEmailService;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class SendEmailVerification
{

    /** @var SendUserEmailService $sendEmailService */
    private $sendEmailService;

    /**
     * Create a new controller instance.
     *
     * @param SendUserEmailService $sendEmailService
     */
    public function __construct(SendUserEmailService $sendEmailService)
    {
        $this->sendEmailService = $sendEmailService;
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisterEvent  $event
     * @return void
     */
    public function handle(UserRegisterEvent $event)
    {
        if ($event->user instanceof MustVerifyEmail && ! $event->user->hasVerifiedEmail()) {
            $this->sendEmailService
                ->sendUserVerify($event->user);
        }
    }
}
