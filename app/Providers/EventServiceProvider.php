<?php

namespace App\Providers;

use App\Events\User\UserForgotPasswordEvent;
use App\Events\User\UserRegisterEvent;
use App\Listeners\Subscription\CreatePayer;
use App\Listeners\User\SendEmailPasswordForgot;
use App\Listeners\User\SendEmailVerification;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserForgotPasswordEvent::class => [
            SendEmailPasswordForgot::class,
        ],
        UserRegisterEvent::class => [
            SendEmailVerification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
