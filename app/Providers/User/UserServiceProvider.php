<?php

namespace App\Providers\User;

use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use App\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserRepositoryInterface::class,
            function () {
                return new UserRepository(
                    $this->app
                        ->make(User::class)
                );
            }
        );
    }
}
