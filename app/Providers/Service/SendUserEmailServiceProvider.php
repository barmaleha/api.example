<?php

namespace App\Providers\Service;

use App\Listeners\User\SendEmailPasswordForgot;
use App\Listeners\User\SendEmailVerification;

use App\Services\Email\SendEmailService;
use App\Services\Email\SendUserEmailService;

use Illuminate\Support\ServiceProvider;

class SendUserEmailServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            SendUserEmailService::class,
            function () {
                return new SendUserEmailService(
                    $this->app
                        ->make(SendEmailService::class)
                );
            }
        );

        $this->app->bind(
            SendEmailPasswordForgot::class,
            function () {
                return new SendEmailPasswordForgot(
                    $this->app
                        ->make(SendUserEmailService::class)
                );
            }
        );

        $this->app->bind(
            SendEmailVerification::class,
            function () {
                return new SendEmailVerification(
                    $this->app
                        ->make(SendUserEmailService::class)
                );
            }
        );
    }
}
