<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(
            'FractalManager',
            function ($app) {
                $manager = (new Manager())
                    ->setSerializer(new ArraySerializer());

                return isset($_GET['include'])
                    ? $manager->parseIncludes($_GET['include'])
                    : $manager;
            }
        );

    }
}
