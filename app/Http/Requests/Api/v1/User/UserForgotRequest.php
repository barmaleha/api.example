<?php

namespace App\Http\Requests\Api\v1\User;

use App\Http\Requests\Api\Request;

class UserForgotRequest extends Request
{

    const EMAIL = 'email';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|null
     */
    public function rules(): ?array
    {
        return [
            self::EMAIL => 'required|string|email',
        ];
    }

}
