<?php

namespace App\Http\Requests\Api\v1\User;

use App\Http\Requests\Api\Request;

class UserRequest extends Request
{

    const NAME  = 'name';

    const EMAIL = 'email';

    const PASS  = 'password';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|null
     */
    public function rules(): ?array
    {
        return [
            self::NAME  => 'required|string|max:255',
            self::EMAIL => 'required|string|email|max:255|unique:users',
            self::PASS  => 'required|string|min:6|confirmed',
        ];
    }

}
