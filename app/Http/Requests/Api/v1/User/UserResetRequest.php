<?php

namespace App\Http\Requests\Api\v1\User;

use App\Http\Requests\Api\Request;

class UserResetRequest extends Request
{

    const TOKEN        = 'token';

    const EMAIL        = 'email';

    const PASS         = 'password';

    const PASS_CONFIRM = 'password_confirmation';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|null
     */
    public function rules(): ?array
    {
        return [
            self::TOKEN => 'required|string',
            self::EMAIL => 'required|string|email',
            self::PASS  => 'required|string|min:6|confirmed',
        ];
    }

}
