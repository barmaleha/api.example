<?php

namespace App\Http\Transformers\User;

use App\Models\User;

use App\Http\Transformers\BaseTransformer;

class UserTransformer extends BaseTransformer
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * User transformer
     *
     * @param  User model $patient
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'          => $user->getKey(),
            'name'        => $user->getName(),
            'email'       => $user->getEmail(),
            'is_verified' => $user->hasVerifiedEmail(),
        ];
    }

}
