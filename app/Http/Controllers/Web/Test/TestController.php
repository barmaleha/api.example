<?php

namespace App\Http\Controllers\Web\Test;

use App\Http\Controllers\Controller;

class TestController extends Controller
{

    /**
     * Test controller
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        dump('test');

        return view('web.dashboard', []);
    }

}
