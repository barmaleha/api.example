<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Services\User\UserService;

use App\Exceptions\User\PasswordNotResetedException;
use App\Exceptions\User\ResetEmailNotSentException;
use App\Exceptions\User\UserNotAuthorizedException;
use App\Exceptions\User\UserNotVerifiedException;

use App\Http\Controllers\Controller;

use App\Http\Requests\Api\v1\User\UserForgotRequest;
use App\Http\Requests\Api\v1\User\UserLoginRequest;
use App\Http\Requests\Api\v1\User\UserRequest;
use App\Http\Requests\Api\v1\User\UserResetRequest;

use App\Http\Transformers\User\UserTransformer;

use App\Models\User;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /** @var UserService User service */
    private $userService;

    /**
     * Create a new UserController instance
     *
     * @param UserService           $userService     User service
     */
    public function __construct(UserService $userService)
    {
        parent::__construct();

        $this->userService = $userService;
    }

    /**
     * Create record of the resource.
     *
     * @param UserRequest           $request
     * @return Response
     */
    public function create(UserRequest $request)
    {
        $user = $this->userService
            ->create($request);

        return response()->item(
            $user,
            new UserTransformer
        );
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  Request              $request
     * @param  User                 $user
     * @return Response
     */
    public function verify(Request $request, User $user)
    {
        $this->userService
            ->verify(
                $request,
                $user
            );

        return response()->item(
            $user,
            new UserTransformer
        );
    }

    /**
     * Handle a login request to the application.
     *
     * @param  UserLoginRequest     $request
     * @return Response
     */
    public function login(UserLoginRequest $request)
    {
        try {
            $user = $this->userService->login($request);
        }
        catch (UserNotVerifiedException $notVerifiedEx) {
            return response()->error(304);
        }
        catch (UserNotAuthorizedException $notAuthEx) {
            return response()->error(305);
        }

        return response()->item(
            $user,
            new UserTransformer
        );
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request              $request
     * @return Response
     */
    public function logout(Request $request)
    {
        $this->userService->logout($request);

        return response()->success(301);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  UserForgotRequest    $request
     * @return Response
     */
    public function forgot(UserForgotRequest $request)
    {
        try {
            $this->userService->forgot($request);
        }
        catch (ResetEmailNotSentException $notSentEx) {
            return response()->error(306);
        }

        return response()->success(302);
    }

    /**
     * Reset password for the given user.
     *
     * @param  UserResetRequest     $request
     * @return Response
     */
    public function reset(UserResetRequest $request)
    {
        try {
            $this->userService->reset($request);
        }
        catch (PasswordNotResetedException $notResetedEx) {
            return response()->error(307);
        }

        return response()->success(303);
    }

}
