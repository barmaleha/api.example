<?php

namespace App\Events\User;

use Illuminate\Queue\SerializesModels;

class UserForgotPasswordEvent
{
    use SerializesModels;

    /**
     * The authenticated user.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    public $user;

    /**
     * The token string.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new event instance.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable   $user
     * @param  string                                       $token
     *
     * @return void
     */
    public function __construct($user, string $token)
    {
        $this->user = $user;
        $this->token = $token;
    }
}
