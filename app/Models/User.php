<?php

namespace App\Models;

use App\Events\User\UserForgotPasswordEvent;
use App\Traits\System\ModelHelper;
use Carbon\Carbon;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as PasswordResetable;
use Illuminate\Contracts\Auth\MustVerifyEmail as Verificable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements Verificable, PasswordResetable
{
    use ModelHelper;
    use CanResetPassword;
    use HasApiTokens;
    use MustVerifyEmail;
    use Notifiable;

    const COLUMN_NAME     = 'name';

    const COLUMN_EMAIL    = 'email';

    const COLUMN_PASS     = 'password';

    const COLUMN_REMEMBER = 'remember_token';

    const COLUMN_VERIFIED = 'email_verified_at';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COLUMN_NAME,
        self::COLUMN_EMAIL,
        self::COLUMN_PASS,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::COLUMN_PASS,
        self::COLUMN_REMEMBER,
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    /**
     * Fire event to send the password reset notification.
     *
     * @param  string  $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        event(new UserForgotPasswordEvent($this, $token));
    }

    /**
     * Returns column value.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(self::COLUMN_NAME);
    }

    /**
     * Sets column value.
     *
     * @param string    $name
     *
     * @return $this    For chained call.
     */
    public function setName(string $name): User
    {
        $this->setAttribute(self::COLUMN_NAME, $name);

        return $this;
    }

    /**
     * Returns column value.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getAttribute(self::COLUMN_EMAIL);
    }

    /**
     * Sets column value.
     *
     * @param string    $email
     *
     * @return $this    For chained call.
     */
    public function setEmail(string $email): User
    {
        $this->setAttribute(self::COLUMN_EMAIL, $email);

        return $this;
    }

    /**
     * Returns column value.
     *
     * @return string
     */
    public function getPass(): string
    {
        return $this->getAttribute(self::COLUMN_NAME);
    }

    /**
     * Sets column value.
     *
     * @param string    $pass
     *
     * @return $this    For chained call.
     */
    public function setPass(string $pass): User
    {
        $this->setAttribute(self::COLUMN_PASS, $pass);

        return $this;
    }

    /**
     * Returns column value.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->getAttribute(self::COLUMN_REMEMBER);
    }

    /**
     * Sets column value.
     *
     * @param string    $token
     *
     * @return $this    For chained call.
     */
    public function setRememberToken($token)
    {
        $this->setAttribute(self::COLUMN_REMEMBER, $token);

        return $this;
    }

    /**
     * Returns column value.
     *
     * @return bool
     */
    public function hasVerifiedEmail(): bool
    {
        return !is_null($this->getAttribute(self::COLUMN_VERIFIED));
    }

    /**
     * Sets column value.
     *
     * @return $this    For chained call.
     */
    public function markEmailAsVerified(): User
    {
        $this->setAttribute(self::COLUMN_VERIFIED, new Carbon);

        return $this;
    }

}
