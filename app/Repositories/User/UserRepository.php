<?php

namespace App\Repositories\User;

use App\Interfaces\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository implements UserRepositoryInterface
{

    /** @var User Model instance */
    protected $model;

    /**
     * Constructor
     *
     * @param User      $model      Provided model instance
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Gets all records
     *
     * @return Collection[User]|null
     */
    public function all(): ?Collection
    {
        return $this->model
            ->all();
    }

    /**
     * Creates new record by given data
     *
     * @param  array    $data       Data of new record
     *
     * @return User|null
     */
    public function create(array $data): ?User
    {
        return $this->model
            ->create($data);
    }

    /**
     * Updates record by given ID nad provided data
     *
     * @param  array    $data       Data for update record
     * @param  int      $id         Given ID of record
     *
     * @return User|null
     */
    public function update(array $data, int $id): ?User
    {
        $record = $this->model
            ->find($id);

        return $record->update($data);
    }

    /**
     * Deletes record by given ID
     *
     * @param  int      $id         Given ID of record
     *
     * @return bool|null
     */
    public function delete(int $id): ?bool
    {
        return $this->model
            ->destroy($id);
    }

    /**
     * Gets record by given ID
     *
     * @param  int      $id         Given ID of record
     *
     * @return User|null
     */
    public function show($id): ?User
    {
        return $this->model
            ->findOrFail($id);
    }

    /**
     * Verifies record by given ID
     *
     * @param  int      $id         Given ID of record
     *
     * @return bool
     */
    public function verify($id): bool
    {
        $record = $this->model
            ->findOrFail($id);

        if (!$record->hasVerifiedEmail()) {
            $record->markEmailAsVerified()
                ->save();

            return true;
        }

        return false;
    }

    /**
     * Reset password by given ID
     *
     * @param  int      $id         Given ID of record
     * @param  string   $password   Given password
     *
     * @return bool
     */
    public function resetPassword(int $id, string $password): bool
    {
        $record = $this->model
            ->findOrFail($id);

        $record->setPass(Hash::make($password))
            ->setRememberToken(Str::random(60))
            ->save();

        return true;
    }
}
